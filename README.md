# SimpleBrowser README #

This README documents whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* Quick summary
This is a simple browser created as one of my assignments during my 4th undergraduate year of BSc Computer Systems at Heriot-Watt University. Project files.rar contains the Visual Studio 2017 project used to create the EXE. This RAR contains password protected folders to prevent plagiarism from students.

### How do I get set up? ###

* Summary of set up
To run the browser, run Coursework 1.exe. There is also a PDF file that contains a description of the project, and a user and developer manual. 

* Dependencies
Make sure that favorites.xml, history.xml, and homepage.xml are in the same directory as Coursework 1.exe.

* Extra Notes
The browser does not render the websites, so it doesn't use CSS, JavaScript, etc, it just loads the HTML script as text.

The browser doesn't have much to show when it comes to design, and there are probably a bunch of bugs in the software as I didn't have a lot of time to focus on those given the assignment deadline, so do keep that in mind ;).